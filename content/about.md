+++
title = "About"
date = "01-05-2021"
aliases = ["about-us","about-hugo","contact"]
[ author ]
  name = "Anxo Sánchez"
+++

Este blog está deseñado e mantido por Anxo Sánchez, profesor da Área de Enxeñaría Química da Universidade de Vigo.